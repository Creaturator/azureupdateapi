using System.Net;
using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;

namespace VersionAPI;

public static class VersionHolder {
    [Function("creatures-in-a-box")]
    public static HttpResponseData ReturnCreaturesVersion(
        [HttpTrigger(AuthorizationLevel.Function, "get")] 
        HttpRequestData req,
        FunctionContext executionContext)
    {
        HttpResponseData res = req.CreateResponse(HttpStatusCode.OK);
        res.Headers.Add("Content-Type", "text/plain; charset=utf-8");
        res.WriteString("Prototype 0.1");
        return res;
    }
}